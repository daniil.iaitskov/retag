#!/usr/bin/env bash

set -e

function err() {
  echo "$@" 1>&2
  exit 1
}

while [ $# -ne 0 ] ; do
  case "$1" in
    -h|--help) cat<<EOF 1>&2
Usage: retag.sh

Makes new GIT tag for HEAD if last tag has format v1.2.3 by appending
suffix ".1" to it.  If latest tag has format v1.2.3.4 then last part
is incremented and new tag would be v1.2.3.5

EOF
               exit 1 ;;
    *) err "Bad option [$1]" ;;
  esac
  shift
done

function retag() {
  CURRENT_TAG=$(git log --format=%d -1 | grep -o -E 'tag: v[0-9.]+' | while read TAG_PREFIX TAG_V ; do
                  echo $TAG_V
                  break
                done)
  if [[ "$CURRENT_TAG" =~ ^(v[0-9]+[.][0-9]+[.][0-9]+[.])([0-9]+)$ ]] ; then
     NEW_TAG="${BASH_REMATCH[1]}$((BASH_REMATCH[2]+1))"
     git tag $NEW_TAG
     git push origin "$NEW_TAG"
  elif [[ "$CURRENT_TAG" =~ ^v[0-9]+[.][0-9]+[.][0-9]+$ ]] ; then
     NEW_TAG="$CURRENT_TAG.1"
     git tag "$NEW_TAG"
     git push origin "$NEW_TAG"
  else
    err "Bad tag format: [$CURRENT_TAG]"
  fi
}

retag
